const Command = require('../structures/Command.js');

class Sleep extends Command {
    constructor(client) {
        super(client, {
            name: "sleep",
            description: "Makes the bot go to sleep for x minutes (default 5, max 60) or wakes the bot up.",
            usage: "sleep | sleep <minutes>"
        });
    }
        goToSleep(minutes, message) {
            this.client.isAsleep = true;
            this.client.logger.log(`Going to sleep for ${minutes} minutes.`)

            setTimeout(() => {
                if (this.client.isAsleep) {
                    this.wakeUp(message);
                }
            }, minutes * 60000);

        }
        wakeUp(message) {
            this.client.logger.log("Waking up!")
            message.channel.send("I'm back and HIGH ENERGY");
            this.client.isAsleep = false;
        }
        
    async run(message, args) {
        if (this.client.isAsleep) {
            this.wakeUp(message);
        }
        else {
            if (args.length === 0) {
                this.goToSleep(5, message);
                message.channel.send("Taking a covfefe break! Be back in 5 minutes!");
            }
            else if (args.length === 1) {
            if (args[0] <= 60 || this.client.permlevel(message) >= 5) {
                    this.goToSleep(args[0], message);
                    message.channel.send(`Taking a covfefe break! Be back in ${args[0]} minutes!`);
                }
                else {
                    message.channel.send('You may only deactivate the bot for up to 60 minutes')
                }
            }
            else {
                message.channel.send("Too many arguments!");
            }
        }
    }
}

module.exports = Sleep;
