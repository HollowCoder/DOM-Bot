exports.run = (client, message) => {

    //Blocks bots, except for the gamechat bot and ignore discordclient.logger channel
    if (message.author.bot) return;

    if (message.content.indexOf(client.config.prefix) === 0) {

        // This is the best way to define args. Trust me.
        const args = message.content.slice(client.config.prefix.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();

        //Fetch user permission level
        const level = client.permlevel(message);

        //Check if the command or alias exists
        const cmd = client.commands.get(command) || client.commands.get(client.aliases.get(command));
        if (!cmd) return;

        //Check if user has permission to run said command
        if (level < cmd.conf.permLevel) {
            message.channel.send("You do not have permission to run this command!");
            return;
        }

        //Logs and runs
        client.logger.log(`${message.author.username} (${message.author.id}) ran command ${cmd.help.name}`, "cmd");
        cmd.run(message, args);
    }
    //Notifies me if Unfollo is mentioned
    if (/unfollo/.test(message.content.toLowerCase())) {
        client.logger.log(`Unfollo was mentioned by ${message.author.tag} in message ${message.content}`);
        const yarden = client.users.find('id', '190902595462758400');
        yarden.send(`Unfollo was mentioned by ${message.author.tag} in message "${message.content}" in #${message.channel.name}"`);
    }
    //Stops running if the channel isn't the designated meme channel
    if (client.config.memeChannel!="MEME CHANNEL: OPTIONAL" && message.channel.id !== client.config.memeChannel) return;
    if (client.isAsleep == true) return;

    //Loop for responses
    var message_content = message.content.toLowerCase();
    let found = false;
    Object.keys(client.dic).forEach(key => {
        var re = new RegExp(`\\b${key}\\b`, 'g');
        if (re.test(message_content) && !found)
        {
            client.logger.cmd(`Found key "${key}" in message "${message_content}" sent by ${message.author.username} (#${message.channel.name})`);
            found = true;
            message.channel.send(client.dic[key]);
        }
    });
}
